<?php
// this include class connection database
include 'connection.php';
//the class products deal with model products
class product extends connection
{
    private $productName;
    private $price;
    private $sku;
    private $typeSwitcher;



    /**
     * Get the value of productName
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * Set the value of productName
     *
     * @return  self
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;

        return $this;
    }

    /**
     * Get the value of price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set the value of price
     *
     * @return  self
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get the value of sku
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set the value of sku
     *
     * @return  self
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get the value of typeSwitcher
     */
    public function getTypeSwitcher()
    {
        return $this->typeSwitcher;
    }

    /**
     * Set the value of typeSwitcher
     *
     * @return  self
     */
    public function setTypeSwitcher($typeSwitcher)
    {
        $this->typeSwitcher = $typeSwitcher;

        return $this;
    }

    //selected to products contain type_switcher == DVD
    public function selectProductsDVD()
    {
        $query = "SELECT
        products.*,
        product_spec.*
    FROM
        `products`
    JOIN product_spec ON products.sku = product_spec.product_sku
    WHERE
        product_spec.spec_name IN('size') ORDER BY products.created_at";
        return $this->runDQL($query);
    }
    //selected to products contain type_switcher == book


    public function selectProductsBook()
    {
        $query = "SELECT
        products.*,
        product_spec.*
    FROM
        `products`
    JOIN product_spec ON products.sku = product_spec.product_sku
    WHERE
        product_spec.spec_name IN('weight') ORDER BY products.created_at";
        return $this->runDQL($query);
    }
    //selected to products contain type_switcher == furniture

    public function selectProductsFurinture()
    {
        $query = "SELECT
        sku,
        product_sku,
        price,
        `name`,
        type_switcher,
        GROUP_CONCAT(`spec_name`),
        GROUP_CONCAT(`value` SEPARATOR 'x') as `values`
    FROM
        `product_spec`
    JOIN products ON products.sku = product_spec.product_sku
    WHERE
        product_spec.spec_name IN('length', 'width', 'height')
    GROUP BY
        product_sku";
        return $this->runDQL($query);
    }

    //this function working of the create product

    public function createProduct()
    {
        $query = "INSERT INTO `products`(`sku`,`name`,price,`type_switcher`)VALUES('$this->sku','$this->productName', $this->price ,'$this->typeSwitcher')";
        return $this->runDML($query);
    }
    //this function working of the delete product


    public function deleteProduct(array $products)
    {
        $array = [];
        foreach ($products as $key => $value) {
            $array[] = $value;
        }
        $array = implode("','", $array);
        $query = "DELETE FROM `products` WHERE `sku` IN ('$array')";

        $this->runDML($query);
    }
}

//the class products deal with model Product_Spec


class ProductSpec extends connection
{
    private $product_sku;
    private $spec_name;
    private $value;

    /**
     * Get the value of product_sku
     */
    public function getProduct_sku()
    {
        return $this->product_sku;
    }

    /**
     * Set the value of product_sku
     *
     * @return  self
     */
    public function setProduct_sku($product_sku)
    {
        $this->product_sku = $product_sku;

        return $this;
    }

    /**
     * Get the value of spec_name
     */
    public function getSpec_name()
    {
        return $this->spec_name;
    }

    /**
     * Set the value of spec_name
     *
     * @return  self
     */
    public function setSpec_name($spec_name)
    {
        $this->spec_name = $spec_name;

        return $this;
    }

    /**
     * Get the value of value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set the value of value
     *
     * @return  self
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
    //this function working 
    public function createvalue()
    {
        $query = "INSERT INTO `product_spec` (`product_sku`, `spec_name`, `value`) VALUES ( '$this->product_sku','$this->spec_name','$this->value')";
        return $this->runDML($query);
    }
}
// this is function working of the redirectoy
function redirctory(){
    return header('location:index.php');
}

if($_POST){
    if (count($_POST) > 3 && isset($_POST['sive'])) {
        // This data is for table prodacu_spec
        $specData = [['size' => $_POST['size']], ['weight' => $_POST['weight']], ['width' => $_POST['width'], 'height' => $_POST['height'], 'length' => $_POST['length']]];
    
        $product = new product;
        $productSpec = new ProductSpec;
    
        $product->setSku($_POST['sku']);
        $product->setProductName($_POST['name']);
        $product->setPrice($_POST['price']);
        $product->setTypeSwitcher($_POST['type_switcher']);
        $product->createProduct();
        foreach ($specData as $in => $va) {
            foreach ($va as $k => $v) {
                if (!empty($v)) {
                    $productSpec->setProduct_sku($_POST['sku']);
                    $productSpec->setSpec_name($k);
                    $productSpec->setValue($v);
                    $productSpec->createvalue();
                }
            };
        }
      redirctory();
    }   
    
}
